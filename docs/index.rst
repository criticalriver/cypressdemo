.. CatCutifier documentation master file, created by
   sphinx-quickstart on Wed Apr 24 15:19:01 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Cypress's API documentation
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

:ref:`genindex`

AWS IoT Structure
====

.. doxygenstruct:: cat
   :members:
   

AWS Set GATT
====

.. doxygenstruct:: SETGATT
   :members:
   
   
AWS Discover GATT
====

.. doxygenstruct:: UPDATEGATT
   :members:
   